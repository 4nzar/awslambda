import { inspect } from "util";

export type LambdaType = Function | string;
export type LambdaStatus = "ok" | "ko";

export interface ErrorResponse {
	statusCode: number,
	description: string;
}

export type LambdaResponseType = {
	status: LambdaStatus;
	data: any;
}

export interface LambdaResponse {
	body: any;
	headers: any;
	statusCode: number;
}

export interface LambdaOptions {
	type:	"event" | "request-response" | "dry-run";
	log:	"none" | "tail";
}

export interface LambdaInvocationOptions {
	FunctionName:	string;
	InvocationType:	"RequestResponse" | "Event" | "DryRun";
	LogType:	"Tail" | "None";
	Payload:	string;
}

export interface LambdaParseResponse {
	type:	"function" | "invocation";
	lambdaFun:	Function | string;
	next:	(string | Function)[];
	data:	string[];
	options?: LambdaInvocationOptions;
}

export class Response {

	public static	ok(context:	any, response:	any, done:	any):	void {
		Response._returnAs(context, response, 200, done);
	}

	public static	ko(context:	any, errorResponse:	ErrorResponse, done:	any):	void {
		Response._returnAs(context, errorResponse, errorResponse.statusCode, done);
	}

	private static	_returnAs(context:	any, result:	any, httpStatusCode:	number, done:	any): void {

		context.callbackWaitsForEmptyEventLoop = false;

		const response: LambdaResponse = {
			body: JSON.stringify(result),
			headers: { 'Access-Control-Allow-Origin': '*' },
			statusCode: httpStatusCode
		};

		done(null, response);
	}
}

