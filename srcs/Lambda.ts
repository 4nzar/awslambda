const jsonpath = require("jsonpath");
import * as AWS                                 from "aws-sdk";
import { Response,
         ErrorResponse,
         LambdaResponse,
         LambdaType,
         LambdaOptions,
         LambdaInvocationOptions,
         LambdaParseResponse }                  from "./Response";
import { DescribeHandler,
         StreamOn }                             from "./annotations";

export { StreamOn };

export class Lambda {

	/*
	**	_extractObject() private static method 
	**
	**	Description:
	**		
	**	Arguments:
	**
	**	Return:
	*/
	private static	_extractData(data: any): any {
		try {
			let obj = JSON.parse(data);
			return (obj);
		} catch (e) {
			return (data);
		}
	}
	
	private static	_parseArguments(...args:	any[]):	LambdaParseResponse {

		let response:	LambdaParseResponse = {
			type: "function",
			data: [],
			next: [],
			lambdaFun: () => {}
		};

		let element:	any = args.shift();
		args = args.filter((val:	any) => val);

		if (args[0] == undefined) {
			response.lambdaFun = element;
			return response;
		}

		if (typeof element != "function") {
			response.type = "invocation";
			response.lambdaFun = element;
		}
		else
			response.lambdaFun = element;

		for (let i in args) {
			if (Array.isArray(args[i]))
				response.data = args[i];
			else
				response.next.push(args[i]);
		}

		return (response);
	}

	private	static	_makeId() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < 5; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}

	private static	_getParametersFromEvent(event:	any):	any {
		let response: any = event.body || event.arguments || event;

		response = Lambda._extractData(response);
		if (event.httpMethod == "GET")
			response = event.queryStringParameters;
		if (!response || response == undefined) {
			if (!event.hasOwnProperty("pathParameters") || !event.pathParameters || event.pathParamters == undefined)
				response = {};
			else
				response = event.pathParameters;
		}
		return (response);
	}

	private static async	_runFunction(lambdaFun:	Function, argumentsList:	any[]):	Promise<any> {
		try {
			let response: any;
			if (!argumentsList || argumentsList == undefined || argumentsList.length == 0)
				response = await lambdaFun();
			else
				response = await lambdaFun(...argumentsList);
			if (response.hasOwnProperty('status') && response.hasOwnProperty('data') && response.hasOwnProperty("_potatoKey"))
				return (response);
			else
				return ({ status: "ok", data: response, _potatoKey: Lambda._makeId() });
		} catch (e) {
			throw e;
		}
	}

	private static	_functionExist(lambdaFun:	string):	Promise<any> {
		return new Promise((resolve, reject) => {
			let lambda = new AWS.Lambda();
			let params = {FunctionName: lambdaFun};
			lambda.getFunction(params, function(error: any) {
				if (error)
					reject({statusCode: 404, message: `Function not found: ${lambdaFun}. ${error}`});
				else
					resolve(true);
			});
		});
	}

	private static	_runInvocation(
		lambdaFun:	string,
		argumentsList:	any[],
		options:	LambdaOptions = {type: "request-response", log: "tail"}):	Promise<any> {
			return new Promise(async (resolve, reject) => {
				AWS.config.update({region: "eu-central-1"});
				try {
					await Lambda._functionExist(lambdaFun);
					let lambda = new AWS.Lambda();
					let invocationType: any = options.type.replace(/(\-\w)/g, (m) => m[1].toUpperCase());
					invocationType = invocationType.charAt(0).toUpperCase() + invocationType.slice(1);
					let logType: any = options.log.charAt(0).toUpperCase() + options.log.slice(1);
					let params: LambdaInvocationOptions = {
						FunctionName: lambdaFun,
						InvocationType: invocationType,
						LogType: logType,
						Payload: JSON.stringify(argumentsList)
					};
					lambda.invoke(params, (error: any, data: any) => {
						if (error) {
							reject(error);
						}
						else {
							let payload = JSON.parse(<string>data.Payload);
							if (payload.hasOwnProperty('status') && payload.hasOwnProperty('data') && payload.hasOwnProperty("_potatoKey"))
								resolve(payload);
							else
								resolve({ status: "ok", data: payload, _potatoKey: Lambda._makeId() });
						}
					});
				} catch (e) {
					reject(e);
				}
			});
	}

	public static	handle(lambdaFun:	LambdaType):	Promise<void>;
	public static	handle(lambdaFun:	LambdaType, data:	string[]):	Promise<void>;
	public static	handle(lambdaFun:	LambdaType, data:	string[], ...nextLambda:	LambdaType[]):	Promise<void>;

	/*
	**	handle() public static method
	**
	**	Description:
	**		
	**	Arguments:
	**
	**	Return:
	*/
//	@DescribeHandler
	public static	handle(...args:	any[]):	any {
		return async (event:	any, context:	any, done:	any):	Promise<void> => {
			try {
				const lambdaInfo:	LambdaParseResponse = Lambda._parseArguments(...args);
				const eventArgs:	any = Lambda._getParametersFromEvent(event);
				const argumentsList:	string[] = lambdaInfo.data.map((argName: string) => jsonpath.value(eventArgs, argName));
				let response: any;
				switch (lambdaInfo.type) {
					case ("function"):
						response = await Lambda._runFunction(<Function>lambdaInfo.lambdaFun, argumentsList);
						break;
					case ("invocation"):
						response = await Lambda._runInvocation(<string>lambdaInfo.lambdaFun, argumentsList);
						break;
					default:
						throw { statusCode: 500, message: "An error occured : Invalid Lambda type." }
				}
				let nextParameters:	any[] = [response, ...argumentsList];
				for (let next of lambdaInfo.next) {
					if (typeof next == "function") {
						response = await Lambda._runFunction(<Function>next, nextParameters);
					}
					else {
						response = await Lambda._runInvocation(<string>next, nextParameters);
					}
					if (Array.isArray(response))
						nextParameters = [...response, ...argumentsList];
					else
						nextParameters = [response, ...argumentsList];
				}
				done(null, { statusCode: 200, body: JSON.stringify(response) });
				//Response.ok(context, response, done);
			} catch (e) {
				let errorResponse:	ErrorResponse = {
					statusCode: e.statusCode || e.code || 500,
					description: e.body || e.message || e.msg || e.errorMessage || e.toString()
				};
				done(null, errorResponse);
				//Response.ko(context, errorResponse, done);
			}
		}
	}
}
