import { diff } from "deep-diff";
import * as AWS from "aws-sdk";

export function DescribeHandler(target: any, key: any, descriptor: any) {
	const originalMethod = descriptor.value;
	descriptor.value = function (...args: any[]) {
		let parameters: any;
		let next: any;

		if (args.length == 1) {
			next = [];
			parameters = "no parameters";
		}
		else {
			next = [...args.slice(2)];
			parameters = args[1];
		}

		for (let i in next)
			next[i] = (typeof next[i] == "function") ? "ƒ()" : "𝝺()";

		console.debug(`Info : ${ target.name } - handler function called`);
		console.debug("\tparameters : ", parameters);
		console.debug("\tnext : ", next);

		return originalMethod.apply(this, arguments);
	}
};

export const StreamOn = (event: "create" | "update" | "delete") =>
	function(target: Object, key: string, descriptor: any) {
		const originalMethod = descriptor.value;
		descriptor.value = function (...args: any[]) {
			console.debug(args);
			let records: any[] = args.shift();
			if (!records || records == undefined || records.length == 0)
				throw Error("No record found");
			if (!records[0].hasOwnProperty("eventName") && !records[0].hasOwnProperty("eventSource"))
				throw Error("No record found");
			let eventName: string = records[0].eventName;
			let eventSource: string = records[0].eventSource;
			if (eventSource != "aws:dynamodb")
				throw Error("invalid aws source");
			if (!["INSERT", "MODIFY", "REMOVE"].includes(eventName))
				throw Error("invalid dynamodb event");
			var response: any;
			var record: any;
			switch (event) {
				case "create":
					if (eventName != "INSERT")
						throw Error(`You cannot use onCreate event with : ${eventName}`);
					record = records.shift();
					response= AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
					break;
				case "update":
					if (eventName != "MODIFY")
						throw Error(`You cannot use onUpdate event with : ${eventName}`);
					response = {};
					response["new"] = AWS.DynamoDB.Converter.unmarshall(records[0].dynamodb.NewImage);
					response["old"] = AWS.DynamoDB.Converter.unmarshall(records[0].dynamodb.OldImage);
					response["diff"] = diff(response["new"], response["old"])
					break;
				case "delete":
					if (eventName != "REMOVE")
						throw Error(`You cannot use onDelete event with : ${eventName}`);
					record = records.shift();
					response= AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
					break;
			}
			return originalMethod.apply(this, [response]);
		}
	}

