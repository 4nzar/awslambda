import { Lambda } from "@sessaidi/lambda";
import { StreamOn } from "@sessaidi/lambda";
import axios from "axios";

class Test {
	constructor() {}

	@StreamOn("update")
	onCreate(data: any): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			resolve(data);
		});
	}
}

let t = new Test();

function t1(data: any){ return data};

export const hello = Lambda.handle(t.onCreate.bind(t), ["Records"])
//export const hello = Lambda.handle(t1, ["Records"])

export const exampleget1 = Lambda.handle(async () => {
        try {
            let response = await axios.get('https://dog.ceo/api/breeds/list/all')
        	return response.data.message;
		} catch (error) {
            throw error;
        }
    },
    ["breed"],
    (...args) => {
		let response: any = args.shift();
		let breeds = response.data;
		args = args.filter((val: any) => val);
		let breed = args[0];
        return breeds[breed] || breeds;
	});

export const exampleget2 = Lambda.handle((email, password, isadmin) => {
		return {
					email : email,
					password : Buffer.from(password).toString('base64'),
					isAdmin: (isadmin == "yes") ? true : false
		};
	}, ["email", "password", "isadmin"]);

export const exampleget3 = Lambda.handle(
	"arn:aws:lambda:region:account-id:function:function-name",
	["value"],
	(response) => {
		console.debug(response);
		response = response.data.map((username) => username.charAt(0).toUpperCase() + username.slice(1).toLowerCase())
		return response;
	},
	(response) => {
		response = response.data.map((username) => `_${username}_`)
		return response;
	});

export const exampleget4 = Lambda.handle((value1, value2, v3) => {
		return `${value1.toUpperCase()} ${value2}`;
	},
	["value1", "value2"],
	(response) => {
		return { response: `_${response.data}_` };
	},
	"arn:aws:lambda:region:account-id:function:function-name",
	(...args) => {
		console.debug(args[0]);
		return args[0].data;
	},
	(response) => {
		return { items : response.data, status: "ok" }
	});

export const exampleget5 = Lambda.handle((value1, value2, v3) => {
		return `${value1.toUpperCase()} ${value2}`;
	},
	["value1", "value2"],
	(response) => {
		throw new Error("an error occured");
		return { response: `_${response.data}_` };
	},
	"arn:aws:lambda:region:account-id:function:function-name",
	(...args) => {
		console.debug(args[0]);
		return args[0].data;
	},
	(response) => {
		return { items : response.data, status: "ok" }
	});


