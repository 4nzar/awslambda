# Lambda

## Installation 
```node
npm install @sessaidi/lambda --save
yarn add @sessaidi/lambda
```
## Type & Interface
### >> Type
```typescript
type LambdaType = Function | string;
```
```typescript
type LambdaResponseType = {
    status: number;
    data: any;
}
```

### >> Interface
```typescript
interface LambdaResponse {
    body: any;
    headers: any;
    statusCode: number;
}
```

## Prototype
```typescript
public static handle(lambdaFun: LambdaType): LambdaResponse;
public static handle(lambdaFun: LambdaType, data: string[]): LambdaResponse;
public static handle(lambdaFun: LambdaType, data: string[], ...nextLambda: LambdaType[]): LambdaResponse;
```

## Usage
### >> simple example
```typescript
import { Lambda }            from "@sessaidi/lambda";

const Hello = Lambda.handle((value: any) => `Hello ${value}`, ["value"]);
```
```typescript
import { Lambda } from "@sessaidi/lambda";

const Hello = Lambda.handle((email, password, isadmin) => {
        return {
            email : email,
            password : Buffer.from(password).toString('base64'),
            isAdmin: (isadmin == "yes") ? true : false
        };
    }, ["email", "password", "isadmin"]);
```
### >>  example with axios
```typescript
import { Lambda }            from "@sessaidi/lambda";
import axios                 from 'axios';

const callmap = Lambda.handle(async () => {
        try {
            let response = await axios.get('https://dog.ceo/api/breeds/list/all')
            return response.data.message;
        } catch (error) {
            throw error;
        }
    },
    ["breed"], // I want to filter response in function of the dog breed
    (...args) => { // since I want to use the parameter breed in this function, need to use rest parameters
        let response: any = args.shift(); //the first element is the response from axios;
        args = args.filter((val: any) => val); // filter arguments to remove undefined and null value
        let breed = args[0]; // get the dog breed from the query
        return response.data[breed] || response.data; // if the dog breed exist return result else return all
    });
```
### >>  example with invocation of lambda function
```typescript
import { Lambda }              from "@sessaidi/lambda";

const callmap = Lambda.handle("arn:aws:lambda:region:account-id:function:function-name",
    [],
    (response: LambdaResponseType) => { //let's imagine that the lambda returns a list of username
        //we want to capitalize all the username in the list
        response = response.data.map((username) => username.charAt(0).toUpperCase() + username.slice(1).toLowerCase())
        return response[breed] || response; // if the dog breed exist return result else return all
    },
    (response: LambdaResponseType) => {
        response = response.data.map((username) => `_${username}_`)
        return response;
    });
```
